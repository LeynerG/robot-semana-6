from Robot import Robot

if __name__ == "__main__":
	print("LIMPIA CASAS")

	r = input("\nNombre del Robot: ")
	robot = Robot(r)

	while True:
		print("\n\nMenu - Robot - " + robot.nombre)
		print("1. Barrer")
		print("2. Fregar")
		print("3. Hablar")
		print("4. Jugar" )
		print("5. Nivel de bateria")
		print("6. Salir")
		try:
			o = int(input("\nOpcion: "))
		except:
			o = -15

		if o == 1:
			if robot.nivel_bateria >=30:
				print(robot.barrer())
			else:
				print("No tengo Bateria")
		elif o == 2:
			if robot.nivel_bateria >=50:
				print(robot.fregar())
			else:
				print("No tengo bateria")
		elif o == 3:
			robot.hablar()
		elif o == 4:
			if robot.nivel_bateria >=20:
				print(robot.jugar())
			else:
				print("No tengo bateria")
		elif o == 5:
				print(robot.nivel_bateria)
		elif o == 6:
				print("Ciao")
	
		else:
				print("Opcion invalida")

